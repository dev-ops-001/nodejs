#!/bin/bash
#update your token and git url in the clone command below
#update your s3 bucket name

 git clone -b develop  https://gitlab+deploy-token-attia:$TOKEN@gitlab.com/dev-ops-001/nodejs.git

 
 cd nodejs
 
 npm install express --save
 cd ../
 apt-get update -y
 apt-get install zip -y
 apt-get install awscli -y
 zip -r nodejs.zip nodejs/ -x '*/node_modules/*' -x '*/.git/*'
 aws s3 nodejs.zip s3://attia-gitlab-staging/
